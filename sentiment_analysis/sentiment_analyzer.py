import json
from watson_developer_cloud import NaturalLanguageUnderstandingV1
import watson_developer_cloud.natural_language_understanding.features.v1 \
    as Features
from watson_developer_cloud import WatsonException
from collections import namedtuple
from sentiment import Sentiment
import nltk
import numpy as np
import re
from nltk.sentiment.vader import SentimentIntensityAnalyzer as SIA


class SentimentAnalyzer(object):

    def calculateSentimentWatson(t):
        try:
            NATURAL_LANGUAGE_UNDERSTANDING = NaturalLanguageUnderstandingV1(
                username="d6b10803-0d0d-4773-922c-e5b0011fd6cd",
                password="Pn4UBHcCfakP",
                version="2017-02-27")

            response = NATURAL_LANGUAGE_UNDERSTANDING.analyze(
                text=t,
                features=[
                    Features.Sentiment()
                ]
            )

            targetScore = float(response['sentiment']['targets'][0]['score'])
            targetLabel = response['sentiment']['targets'][0]['label']
            documentScore = float(response['sentiment']['document']['score'])
            documentLabel = response['sentiment']['document']['label']
        except Exception as e:
            print(e)
            return Sentiment(0, 'not known', 0, 'not known')

        return Sentiment(targetScore, targetLabel, documentScore, documentLabel)

    def calculateSentimentNLTK(t):
        sia = SIA()
        docScore = sia.polarity_scores(str(t))['compound']
        if docScore > 0.2:
            docLabel = 'positive'
        elif docScore < -0.2:
            docLabel = 'negative'
        else:
            docLabel = 'neutral'
        return Sentiment(targetScore=0, targetLabel='none', documentScore=docScore, documentLabel=docLabel)

    def calculateKewords(t):
        natural_language_understanding = NaturalLanguageUnderstandingV1(
            username="d6b10803-0d0d-4773-922c-e5b0011fd6cd",
            password="Pn4UBHcCfakP",
            version="2017-02-27")

        response = natural_language_understanding.analyze(
            text=t,
            features=[
                Features.Keywords(
                    # Keywords options
                    sentiment=False,
                    emotion=False,
                    limit=5
                )
            ]
        )

        keywords = []
        for keyword in response['keywords']:
            keywords.append(keyword)
       

        return keywords