import MySQLdb


class Database:
    def __init__(self):
        """
         self.db = MySQLdb.connect(host="127.0.0.1",user="root", passwd="SeiLuk123", db="coins", charset="utf8", use_unicode=True)
        """
        self.db = MySQLdb.connect(host="fakenews.cvj9c86ao2su.eu-central-1.rds.amazonaws.com",
                                  user="fakenews_db", passwd="coins1234", db="fakenews", charset="utf8", use_unicode=True)

    def writeArticleToDB(self, date, newspaper ,url, heading, text):
        cursor = self.db.cursor()
        try:
            cursor.execute(
                """INSERT INTO news_article (date, newspaper, url, heading, content)
                    VALUES(STR_TO_DATE(%s, '%%Y-%%m-%%d'), %s, %s, %s, %s) """,
                    (date ,newspaper, url, heading, text)
                    )
            self.db.commit()
        except:     
            self.db.rollback()

        cursor.close()

    def readNewsArticles(self):
        cursor = self.db.cursor()
        cursor.execute(
        """SELECT *
            FROM news_article
            WHERE newspaper LIKE 'Washington Post'
            AND date < '2016-01-01' AND date > '2015-09-01'
            ORDER BY date DESC
        """
            )
        news_articles = cursor.fetchall()
        cursor.close()
        return news_articles


    def updateNewsArticle(self, articleId, articleText):
        try:
            cursor = self.db.cursor()
            cursor.execute(
                """UPDATE news_article
                    SET content=%s
                    WHERE id= %s""", (articleText, articleId)
            )
            self.db.commit()
        except Exception as e: 
            print(e)
            self.db.rollback()
        cursor.close()


    def writeKeyword(self, text, relevance ,articleId):
        cursor = self.db.cursor()
        try:
            cursor.execute(
                """INSERT INTO keyword (text, relevance, article_id)
                    VALUES(%s, %s, %s)""",
                    (text ,relevance, articleId)
                    )
            self.db.commit()
        except:     
            self.db.rollback()

        cursor.close()