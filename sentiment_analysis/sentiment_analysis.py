from sentiment_analyzer import SentimentAnalyzer
from database import Database

db = Database()
articles = db.readNewsArticles()


for article in articles:
    content = article[5]
    if content != '' and len(content) > 10:
        keywords =  SentimentAnalyzer.calculateKewords(article[5])
        print(article[0], article[1])
        for keyword in keywords:
            db.writeKeyword(keyword['text'], keyword['relevance'], article[0])
   
