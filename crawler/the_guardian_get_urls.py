import urllib.request
import json
import database 

for i in range(1,90):
    url = 'http://content.guardianapis.com/search?page=' + str(i) +'&page-size=200&q=amazon&api-key=eecd7240-7d6a-4aba-b121-7e1b747f7c28';
    jsonResponse = json.loads(urllib.request.urlopen(url).read())

    results = jsonResponse['response']['results']

    db = database.Database()

    for result in results:
        if result['type'] == 'article':
            date = result['webPublicationDate'].split('T', 1)[0]
            print(result['webUrl'])
            db.writeArticleToDB(
            date,
            'The Guardian',
            result['webUrl'],
            result['webTitle'],
            '' 
            )  
    