from bs4 import BeautifulSoup as bs
from urllib.request import urlopen
from robobrowser import RoboBrowser
import database

def scrapeUrl(articleUrl):
    browser.open(articleUrl)
    text = getText()
    return text


def getText():
    try:
        html = bs(str(browser.parsed), "html.parser")
        article = html.body.findAll('div', {'class' : 'content__article-body from-content-api js-article__body'})
        paragraphs = article[0].findAll('p')
        text = ''
        for i in range(len(paragraphs)):
            text += paragraphs[i].text
    except Exception:
        text = ''

    return text

browser = RoboBrowser(history=False)

db = database.Database()
articles = db.readNewsArticles()

for i in range(len(articles)):
    content = scrapeUrl(articles[i][3])
    hasContent = False
    contentLength = 0
    if content != '':
        hasContent = True
        contentLength = len(content)

    
    print(articles[i][0], articles[i][1], hasContent, contentLength)
    db.updateNewsArticle(articles[i][0], content)
   

